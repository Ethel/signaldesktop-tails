# Signaldesktop-tails

## Signal sous Tails sans Smartphone
Le but de ce répertoire est double:

fournir de la doc sur comment installer/utiliser Signal sous Tails et plus particulièrement sans Smartphone
Fournir des scripts pour automatiser l'installation, les mises à jours, etc.

## Préalable
Avoir créé dans Tails une partition persistente et l'avoir configurée pour conserver :
	- Les logiciels supplémentaires
	- Les Doftiles 

Pour recevoir le code d'activation de Signal, être en possession soit d'un téléphone et d'une carte SIM, soit d'un numéro en ligne.

## Installation
Démarrer Tails, définir un mot de passe d'administration et devérrouiller la persistence chiffrée.

### Télécharger le dépot
### Lancer l'installation
Ouvrir le dossier, faire un clic droit dans le vide, et ouvrir dans un terminal. Puis tapez :

```console
./signal-tails.sh
```

### Configurer un nouveau compte signal
L'installation faite, il faut lier `signal-desktop` à un numéro de téléphone. 

#### Si vous avez un telephone et une carte SIM

#### Si vous utilisez un fournisseur de numero en ligne

## Maintenance
Pour mettre à jour, supprimer la configuration (pour changer de numero par exmeple), ou desinstaller completement signal : 
Ouvrir un terminal dans le dossier contenant le script et le relancer en tapant : 

```console
./signal-tails.sh
```

Suivre le script

## La suite 
Faire tester le script par différentes personnes
Fiabliliser le script d'installation, notamment en cas de bug du réseau.
Prevoir des sorties en fonction des erreurs
Prevoir de purger en cas de bug du script au milieu de l'install
Verifier l'existence d'une install precedente avant de lancer les commandes.
Faire un outil de mise a jour qui verifie d'abord les versions en cours. Peut-être aussi verifier la version de JAVA necessaire et verifier si il faut le mettre
