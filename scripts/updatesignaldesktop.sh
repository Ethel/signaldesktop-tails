#! /usr/bin/env bash

wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
sudo bash -c "
cat signal-desktop-keyring.gpg | tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] tor+https://updates.signal.org/desktop/apt xenial main' |\
tee -a /etc/apt/sources.list.d/signal-xenial.list
apt update
"
apt download signal-desktop
mkdir -p ~/Applications/signal-desktop
dpkg-deb -xv $(ls signal-desktop*.deb) ~/Applications/signal-desktop

wget https://0xacab.org/jc7v/signal-sous-tails/-/raw/master/code/launchers/Signal.desktop?inline=false -O /home/amnesia/.local/share/applications/Signal.desktop
cp ~/.local/share/applications/Signal.desktop /live/persistence/TailsData_unlocked/dotfiles/.local/share/applications/

cp -r ~/Applications/ /live/persistence/TailsData_unlocked/dotfiles/
echo "Signal a été mis à jour!"

