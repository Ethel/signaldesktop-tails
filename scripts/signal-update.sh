#!/bin/bash

BOLD=$(tput bold)
STD=$(tput sgr0)

# Define Variables

SCRIPT_DIR=$1
DOTFILES=$2

# Update installer
# git pull

# Create directories
mkdir -pv data/dynamic/signal-cli/version-check/
mkdir -pv data/dynamic/signal-desktop/

## Find Signal-cli latest version
while true;do
	wget --spider https://github.com/AsamK/signal-cli/releases/latest/ 2> data/dynamic/signal-cli/version-check/v && break
done
SCV=$(cat data/dynamic/signal-cli/version-check/v | grep "signal-cli/releases/tag/v" | head -n1)
echo $SCV > data/dynamic/signal-cli/version-check/vs
SCV=$(cat data/dynamic/signal-cli/version-check/vs | grep -G -o 'v[0-9].\S*')
echo $SCV > data/dynamic/signal-cli/version-check/vs0
SCV0=$(cat data/dynamic/signal-cli/version-check/vs0 | grep -G -o '[0-9].\S*')
rm -rf data/dynamic/signal-cli/version-check
echo -e "${BOLD}Most recent version of signal-cli seems to be ${SCV0} ${STD}"

## Download Signal-cli
if [ -f "data/dynamic/signal-cli/signal-cli.tar.gz" ]; 
	then
		echo -e "${BOLD}signal-desktop est déjà présent, pas besoin de le télécharger !${STD}"
	else
		echo -e "${BOLD}Downloading signal-cli-${SCV0}.${STD}"
		cd data/dynamic/signal-cli/
		while true;do
			wget -O signal-cli.tar.gz https://github.com/AsamK/signal-cli/releases/download/$SCV/signal-cli-$SCV0-Linux.tar.gz && break
		done
		cd ../../../
		echo -e "${BOLD}signal-desktop a été téléchargé avec succès !${STD}"
fi

# Get signal-desktop repository key
echo -e "Get signal-desktop key"
cd data/dynamic/signal-desktop/
while true;do
	wget -T 15 -q -O - https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg && break
done
cd ../../../

# Add apt sources and install dependencies
echo -e "${BOLD}adding sources an installing dependencies, hit your password${STD}"
sudo ./scripts/sudo-update-commands.sh

## Download Signal-Desktop
echo -e "${BOLD}Downloading Signal-Desktop.${STD}"
cd data/dynamic/signal-desktop/
while true;do
	apt download signal-desktop && break
done
cd ../../../
echo -e "${BOLD}signal-desktop a été téléchargé avec succès !${STD}"

# Remove filesystem
rm -rf $DOTFILES/.local/share/applications/Signal*
rm -rf $DOTFILES/Applications/signal-scripts
rm -rf $DOTFILES/Applications/signal-desktop
rm -rf /home/amnesia/Applications/signal-desktop
rm -rf /home/amnesia/Applications/signal-cli*
rm -rf $DOTFILES/Applications/signal-cli*

# Recreate filesystem for installation
mkdir -pv data/dynamic/signal-cli/version-check/
mkdir -pv data/dynamic/signal-desktop/
mkdir -pv $DOTFILES/.local/share/applications/
mkdir -pv /home/amnesia/.local/share/applications/
mkdir -pv $DOTFILES/Applications/signal-scripts/
mkdir -pv $DOTFILES/Applications/signal-desktop/
mkdir -pv /home/amnesia/Applications/

# Copy files to system 
cp -v $SCRIPT_DIR/data/static/launchers/* $DOTFILES/.local/share/applications/
cp -v $SCRIPT_DIR/data/static/launchers/* /home/amnesia/.local/share/applications/

# Move scripts to applications/signal-scripts
echo -e "${BOLD}Copie des scripts de Signal${STD}"
cp -v $SCRIPT_DIR/data/static/scripts/startup.sh $DOTFILES/Applications/signal-scripts/
cp -v $SCRIPT_DIR/data/static/scripts/new-conversation.py $DOTFILES/Applications/signal-scripts/
chmod +x $DOTFILES/Applications/signal-scripts/

# Install signal-desktop
cd data/dynamic/signal-desktop/
dpkg-deb -xv $(ls signal-desktop*.deb) $DOTFILES/Applications/signal-desktop
while : ; do
	if [ $? -eq 0 ]; then
		echo "${BOLD}Signal-Desktop a été installé.${STD}"
		break
	fi
	echo -e "${RED}Echec de l'installation de Signal-Desktop${STD}"
	exit
done
cd ../../../

# Install Signal-cli
tar -zxvf data/dynamic/signal-cli/signal-cli.tar.gz -C /home/amnesia/Applications/
cp -rv /home/amnesia/Applications/signal-cli-$SCV0 $DOTFILES/Applications/

echo -e "export JAVA_TOOL_OPTIONS=\"-Djava.net.preferIPv4Stack=true\"\nalias signal-cli=\"torsocks ~/Applications/signal-cli-$SCV0/bin/signal-cli\"" >> ~/.bashrc
cp -v ~/.bashrc $DOTFILES/

cp -rv $DOTFILES/Applications/signal-scripts ~/Applications

# Pour l'update de .bashrc : https://unix.stackexchange.com/questions/438105/remove-certain-characters-in-a-text-file

# Clean-up 

# srm -rf data/dynamic/*
